package com.softvision.jetpacksecuritydemo

class Secrets {

    //Method calls will be added by gradle task hideSecret
    //Example : external fun getWellHiddenSecret(packageName: String): String

    companion object {
        init {
            System.loadLibrary("secrets")
        }
    }

    external fun getnative_api_key(packageName: String): String

    external fun getnative_api_key(packageName: String): String

    external fun getnative_api_key(packageName: String): String

    external fun getnative_api_key(packageName: String): String

    external fun getnative_api_key(packageName: String): String

    external fun getnative_api_key(packageName: String): String

    external fun getnativeApiKey(packageName: String): String
}