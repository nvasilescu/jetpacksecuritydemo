#include "secrets.hpp"

#include <jni.h>

#include "sha256.hpp"
#include "sha256.cpp"

/* Copyright (c) 2020-present Klaxit SAS
*
* Permission is hereby granted, free of charge, to any person
* obtaining a copy of this software and associated documentation
* files (the "Software"), to deal in the Software without
* restriction, including without limitation the rights to use,
* copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the
* Software is furnished to do so, subject to the following
* conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
* OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
* HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
* OTHER DEALINGS IN THE SOFTWARE.
*/

char *customDecode(char *str) {
    /* Add your own logic here
    * To improve your key security you can encode it before to integrate it in the app.
    * And then decode it with your own logic in this function.
    */
    return str;
}

jstring getOriginalKey(
        char *obfuscatedSecret,
        int obfuscatedSecretSize,
        jstring obfuscatingJStr,
        JNIEnv *pEnv) {

    // Get the obfuscating string SHA256 as the obfuscator
    const char *obfuscatingStr = pEnv->GetStringUTFChars(obfuscatingJStr, NULL);
    char buffer[2 * SHA256::DIGEST_SIZE + 1];

    sha256(obfuscatingStr, buffer);
    const char *obfuscator = buffer;

    // Apply a XOR between the obfuscated key and the obfuscating string to get original string
    char out[obfuscatedSecretSize + 1];
    for (int i = 0; i < obfuscatedSecretSize; i++) {
        out[i] = obfuscatedSecret[i] ^ obfuscator[i % strlen(obfuscator)];
    }

    // Add string terminal delimiter
    out[obfuscatedSecretSize] = 0x0;

    //(Optional) To improve key security
    return pEnv->NewStringUTF(customDecode(out));
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_softvision_jetpacksecuritydemo_Secrets_getnative_api_key(
        JNIEnv *pEnv,
        jobject pThis,
        jstring packageName) {
    char obfuscatedSecret[] = { 0x57, 0x7, 0x43, 0x5f, 0x4e, 0x0, 0xb, 0x65, 0xd, 0x66, 0x57, 0x44, 0x77, 0x55, 0x59, 0x6d, 0x59, 0x4, 0x8, 0x53, 0x41, 0x26, 0x13, 0x20, 0xb, 0x1, 0x2e, 0x56, 0x7d, 0x10, 0x6d, 0x50, 0x15, 0x6e, 0x63, 0x58, 0x77, 0x72, 0x53, 0xb, 0x4e, 0x67, 0x22, 0x5b, 0x76, 0x6f, 0x21, 0x4d, 0x8, 0x1, 0x24, 0x19, 0x55, 0x4b, 0x34, 0x40, 0x60, 0x65, 0x1c, 0x44, 0x2c, 0x43, 0x4, 0x52, 0x5d, 0x23 };
    return getOriginalKey(obfuscatedSecret, sizeof(obfuscatedSecret), packageName, pEnv);
}

extern "C"
JNIEXPORT jstring JNICALL
Java_com_softvision_jetpacksecuritydemo_Secrets_getnativeApiKey(
        JNIEnv* pEnv,
        jobject pThis,
        jstring packageName) {
     char obfuscatedSecret[] = { 0x57, 0x7, 0x43, 0x5f, 0x4e, 0x0, 0xb, 0x65, 0xd, 0x66, 0x57, 0x44, 0x77, 0x55, 0x59, 0x6d, 0x59, 0x4, 0x8, 0x53, 0x41, 0x26, 0x13, 0x20, 0xb, 0x1, 0x2e, 0x56, 0x7d, 0x10, 0x6d, 0x50, 0x15, 0x6e, 0x63, 0x58, 0x77, 0x72, 0x53, 0xb, 0x4e, 0x67, 0x22, 0x5b, 0x76, 0x6f, 0x21, 0x4d, 0x8, 0x1, 0x24, 0x19, 0x55, 0x4b, 0x34, 0x40, 0x60, 0x65, 0x1c, 0x44, 0x2c, 0x43, 0x4, 0x52, 0x5d, 0x23 };
     return getOriginalKey(obfuscatedSecret, sizeof(obfuscatedSecret), packageName, pEnv);
}
