package com.softvision.jetpacksecuritydemo.encrypted

interface IEncryptedSharedPrefs {
    fun savePassword(password: String)

    fun getPassword(): String
}