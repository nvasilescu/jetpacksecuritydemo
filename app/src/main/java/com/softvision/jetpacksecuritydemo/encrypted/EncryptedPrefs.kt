package com.softvision.jetpacksecuritydemo.encrypted

import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.softvision.jetpacksecuritydemo.App

const val EMPTY_STRING = ""

class EncryptedPrefs(masterKey: MasterKey, sharedPrefsName: String) : IEncryptedSharedPrefs {
  
  private val encryptedSharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
    App.instance,
    sharedPrefsName,
    masterKey,
    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
  )
  
  override fun savePassword(password: String) {
    encryptedSharedPreferences.edit().putString(PASSWORD_KEY, password).apply()
  }
  
  override fun getPassword(): String {
    return encryptedSharedPreferences.getString(PASSWORD_KEY, EMPTY_STRING) ?: EMPTY_STRING
  }

  companion object {
    private const val PASSWORD_KEY = "PASSWORD"
  }
}