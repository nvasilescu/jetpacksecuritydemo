package com.softvision.jetpacksecuritydemo.ui.login

import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.softvision.jetpacksecuritydemo.BuildConfig
import com.softvision.jetpacksecuritydemo.R
import com.softvision.jetpacksecuritydemo.Secrets
import com.softvision.jetpacksecuritydemo.databinding.ActivityLoginBinding


private const val TAG = "SECRET_TAG"

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding

    private val SHARED_PREFS_NAME = "SHARED_PREFS_NAME"

    private val SECRET_API_KEY_NOT_NEEDED_AT_COMPILE_TIME = "jetpack9QoP1rAmlZm2idtBwBo6MfMsTdsYWjAJaovVCcCZEzl0BzlrRvXUzwMu1bdE"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.metaDataButton?.setOnClickListener { metaDataApiKey() }
        binding.stringResourceButton?.setOnClickListener { resourceApiKey() }
        binding.localProperties?.setOnClickListener { localProperties() }
        binding.googleSecretsPlugin?.setOnClickListener { googleSecretsPlugin() }
        binding.nativeObfuscation?.setOnClickListener { nativeObfuscation() }
        binding.jetpackSecurityWrite?.setOnClickListener { saveSecretInEncryptedSharedPreferences() }
        binding.jetpackSecurityRead?.setOnClickListener { readSecretInEncryptedSharedPreferences() }
        binding.ciCD?.setOnClickListener { systemEnvVars() }

        //1 meta data
        //metaDataApiKey()

        //2 resource files
        //resourceApiKey()

        //3 local properties
        //localProperties()
        //googleLocalProperties()

        //4 native obfuscation
        //nativeObfuscation()

        //5 jetpack security
        //jetpackSecurity()

        //6 system env vars
        //systemEnvVars()
    }


    private fun metaDataApiKey() {
        packageManager.getApplicationInfo(
            packageName,
            PackageManager.GET_META_DATA
        ).also { appInfo ->
            appInfo.metaData.getString("secret_api_key")?.let {
                Log.d(TAG, "META DATA api key = $it")
            } ?: run {
                Log.d(TAG, "api key not found")
            }
        }
    }

    private fun resourceApiKey() {
        val apiKey = getString(R.string.secret_api_key)
        Log.d(TAG, "RESOURCES api key = $apiKey")
    }

    private fun localProperties() {
        val apiKey = BuildConfig.SECRET_API_KEY
        Log.d(TAG, "Local Properties api key = $apiKey")
    }

    private fun googleSecretsPlugin() {
        val apiKey = BuildConfig.google_api_key
        Log.d(TAG, "Google Local Properties api key = $apiKey")

        packageManager.getApplicationInfo(
            packageName,
            PackageManager.GET_META_DATA
        ).also { appInfo ->
            appInfo.metaData.getString("google_api_key")?.let {
                Log.d(TAG, "Google META DATA api key = $it")
            } ?: run {
                Log.d(TAG, "api key not found")
            }
        }
    }

    private fun nativeObfuscation() {
        val apiKey = Secrets().getnativeApiKey(packageName)
        Log.d(TAG, "NATIVE api key = $apiKey")
    }


    private fun saveSecretInEncryptedSharedPreferences(){
        val masterKey = MasterKey.Builder(this).apply {
            setKeyScheme(MasterKey.KeyScheme.AES256_GCM)

            //biometrics required - number of seconds that the key should remain accessible after user  authentication.
            //default is 300 seconds
            //throws android.security.keystore.UserNotAuthenticatedException if used without auth
            setUserAuthenticationRequired(false)

            //Allow the key to be stored on a separate hardware module - Api 28 or higher
            setRequestStrongBoxBacked(true)
        }.build()


        val encryptedSharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            this,
            SHARED_PREFS_NAME,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        encryptedSharedPreferences.edit().putString("secret_api_key", SECRET_API_KEY_NOT_NEEDED_AT_COMPILE_TIME).commit()

    }

    private fun readSecretInEncryptedSharedPreferences() {
        val masterKey = MasterKey.Builder(this).apply {
            setKeyScheme(MasterKey.KeyScheme.AES256_GCM)

            //biometrics required - number of seconds that the key should remain accessible after user  authentication.
            //default is 300 seconds
            //throws android.security.keystore.UserNotAuthenticatedException if used without auth
            setUserAuthenticationRequired(false)

            //Allow the key to be stored on a separate hardware module - Api 28 or higher
            setRequestStrongBoxBacked(true)
        }.build()


        val encryptedSharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            this,
            SHARED_PREFS_NAME,
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        val apiKey = encryptedSharedPreferences.getString("secret_api_key", null);
        Log.d(TAG, "jetpack api key = $apiKey")
    }

    private fun systemEnvVars(){
        val apiKey = BuildConfig.CI_API_KEY
        Log.d(TAG, "System env api key = $apiKey")
    }
}